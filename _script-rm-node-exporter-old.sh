#!/bin/bash

sudo /usr/bin/systemctl stop node-exporter
sudo /usr/bin/systemctl disable node-exporter
sudo /usr/bin/systemctl stop node_exporter
sudo /usr/bin/systemctl disable node_exporter

sudo rm -rf /etc/systemd/system/node-exporter.service
sudo rm -rf /etc/systemd/system/node_exporter.service
sudo /usr/bin/systemctl daemon-reload
sudo /usr/bin/systemctl reset-failed

sudo userdel -r node-exporter
sudo userdel -r nodeusr
sudo rm -rf /usr/local/bin/node_exporter
sudo rm -rf /opt/bootstrap/node_exporter-1.1.1.linux-amd64/node_exporter
sudo rm -rf /opt/bootstrap/node_exporter-1.1.1.linux-amd64.tar.gz
